To automatically update zotero.bib from zotero, follow the instructions at [here](https://www.youtube.com/watch?v=LuXtWZrgMII).
Remember to use Better BibTex instead of Better BibLatex, uncheck Export Notes, and check Keep updated.
